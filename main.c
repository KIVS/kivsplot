/*
 * main.c
 *
 *  Created on: 17.10.2013
 *      Author: Marc
 */

#include <stdio.h>

#define GRAPHNAME "Ping"
#define XNAME "Sample Number"
#define YNAME "Ping time"

void gnpl()
{
	/*
	 * plotting with gnuplot is pretty simple:
	 * open a pipe, push your commands through it and finally close it
	 * (this causes gnuplot to receive the EOF character and exit)
	 */
	FILE* pipe = popen("gnuplot -persist","w");
	fprintf(pipe, "set boxwidth 0.9 relative\n");
	fprintf(pipe, "set xtics auto\n");
	fprintf(pipe, "set ytics auto\n");
	fprintf(pipe, "set terminal png\n");
	fprintf(pipe, "set output 'graph.png'\n");
	fprintf(pipe, "set title '%s'\n", GRAPHNAME);
	fprintf(pipe, "set xlabel '%s'\n", XNAME);
	fprintf(pipe, "set ylabel '%s'\n", YNAME);
	fprintf(pipe, "plot for [col=2:4] 'out.csv' u 18:col w l title columnheader\n");
	fclose(pipe);
	return;
}

int main(){
	gnpl();
	return 0;
}
